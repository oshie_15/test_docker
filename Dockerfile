FROM ruby:3.2.2-alpine

RUN apk update && apk add build-base ruby-full yarn nodejs sqlite-dev tzdata

RUN mkdir /app

WORKDIR /app

COPY Gemfile /app/Gemfile
COPY Gemfile.lock /app/Gemfile.lock

RUN bundle install

ENV RAILS_ENV production

COPY . .

EXPOSE 4040

CMD rails db:migrate RAILS_ENV=production assets:precompile && rails server -b '0.0.0.0'